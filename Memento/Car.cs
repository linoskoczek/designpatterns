namespace Memento
{
    public class Car
    {
        public string Name { get; set; }
        public int Mileage { get; set; }
        public double Value { get; set; }

        public void Drive(int kilometers)
        {
            Mileage += kilometers;
            Value -= 0.05 * kilometers;
        }

        public CarMemento SaveToMemento()
        {
            return new CarMemento(Name, Mileage, Value);
        }

        public void RestoreFromMemento(CarMemento memento)
        {
            Name = memento.Name;
            Mileage = memento.Mileage;
            Value = memento.Value;
        }

        public override string ToString()
        {
            return "I am a " + Name + " with " + Mileage + " kilometers done. My value is estimated to be " + Value;
        }

        public class CarMemento
        {
            public CarMemento(string name, int mileage, double value)
            {
                Name = name;
                Mileage = mileage;
                Value = value;
            }

            public string Name { get; }
            public int Mileage { get; }
            public double Value { get; }

            public override string ToString()
            {
                return "I was a " + Name + " with " + Mileage + " kilometers done. My value was estimated to be " +
                       Value;
            }
        }
    }
}