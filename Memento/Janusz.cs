namespace Memento
{
    public class Janusz
    {
        public Car.CarMemento CarMemento;
        public Car Car { get; set; }

        public void BuyCar()
        {
            Car = new Car
            {
                Name = "Volkswagen Passat B5",
                Value = 10000.0,
                Mileage = 260000
            };
            RememberCar();
        }

        public void RememberCar()
        {
            CarMemento = Car.SaveToMemento();
        }

        public void PrepareCarForSell()
        {
            Car.RestoreFromMemento(CarMemento);
        }
    }
}