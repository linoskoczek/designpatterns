﻿using System;

namespace Memento
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var janusz = new Janusz();

            Console.WriteLine("Let's buy a car...");
            janusz.BuyCar();
            Console.WriteLine(janusz.Car + "\n");

            janusz.Car.Drive(10000);
            janusz.Car.Drive(40000);
            janusz.Car.Drive(35000);

            Console.WriteLine("After some time...");
            Console.WriteLine(janusz.Car);
            Console.WriteLine("I hope one day my Passat will be as good as it was when I bought it...");
            Console.WriteLine(janusz.CarMemento + "\n");

            Console.WriteLine("Let's sell my car...");
            Console.WriteLine(janusz.Car + "\n");
            Console.WriteLine("...but let's make it a bit better" + "\n");

            janusz.PrepareCarForSell();
            Console.WriteLine(janusz.Car);

            Console.WriteLine("Now it's better! :)");
            Console.WriteLine("SELL VW PASSAT PERFECT STATE FOR " + (janusz.Car.Value + 1500) + " PLN!");
        }
    }
}