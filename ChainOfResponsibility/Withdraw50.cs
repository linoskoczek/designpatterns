namespace Task45
{
    public class Withdraw50 : Withdraw
    {
        public Withdraw50() : base(50)
        {
            Successor = new Withdraw100();
        }
    }
}