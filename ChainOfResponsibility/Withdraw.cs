using System;

namespace Task45
{
    public abstract class Withdraw
    {
        protected Withdraw(int singleBillNoteValue)
        {
            SingleBillNoteValue = singleBillNoteValue;
        }

        protected internal int SingleBillNoteValue { get; }
        protected Withdraw Successor { get; set; }

        protected internal virtual int MakeWithdrawal(int amount)
        {
            if (amount > SingleBillNoteValue && amount > Successor.SingleBillNoteValue)
                amount = Successor.MakeWithdrawal(amount);

            var numberOfBillNotes = amount / SingleBillNoteValue;
            amount -= numberOfBillNotes * SingleBillNoteValue;

            if (numberOfBillNotes > 0)
                Console.WriteLine("[-->] " + numberOfBillNotes + " x " + SingleBillNoteValue);
            return amount;
        }
    }
}