using System;

namespace Task45
{
    public class Withdraw200 : Withdraw
    {
        public Withdraw200() : base(200)
        {
        }

        protected internal override int MakeWithdrawal(int amount)
        {
            var numberOfBillNotes = amount / SingleBillNoteValue;
            amount -= numberOfBillNotes * SingleBillNoteValue;

            Console.WriteLine("[-->] " + numberOfBillNotes + " x " + SingleBillNoteValue);
            return amount;
        }
    }
}