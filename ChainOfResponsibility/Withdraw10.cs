using System;

namespace Task45
{
    public class Withdraw10 : Withdraw
    {
        public Withdraw10() : base(10)
        {
            Successor = new Withdraw20();
        }

        protected internal override int MakeWithdrawal(int amount)
        {
            if (amount % SingleBillNoteValue != 0)
            {
                Console.WriteLine("[ ! ] Amount of money is indivisible by " + SingleBillNoteValue + "!");
                return -1;
            }

            if (amount > SingleBillNoteValue && amount > Successor.SingleBillNoteValue)
                amount = Successor.MakeWithdrawal(amount);

            var numberOfBillNotes = amount / SingleBillNoteValue;
            amount -= numberOfBillNotes * SingleBillNoteValue;

            if (numberOfBillNotes > 0)
                Console.WriteLine("[-->] " + numberOfBillNotes + " x " + SingleBillNoteValue);
            return amount;
        }
    }
}