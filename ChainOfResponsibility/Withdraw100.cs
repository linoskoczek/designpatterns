namespace Task45
{
    public class Withdraw100 : Withdraw
    {
        public Withdraw100() : base(100)
        {
            Successor = new Withdraw200();
        }
    }
}