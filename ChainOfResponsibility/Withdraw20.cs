namespace Task45
{
    public class Withdraw20 : Withdraw
    {
        public Withdraw20() : base(20)
        {
            Successor = new Withdraw50();
        }
    }
}