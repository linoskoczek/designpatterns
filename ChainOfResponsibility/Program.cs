﻿using System;
using System.Diagnostics;

namespace Task45
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            for (var myHajs = 10; myHajs < 500; myHajs += 20)
            {
                Console.WriteLine("\nMe wanna withdraw " + myHajs);
                var result = new Withdraw10().MakeWithdrawal(myHajs);
                Debug.Assert(result == 0);
            }
        }
    }
}