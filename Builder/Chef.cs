using Builder.Pizzas;

namespace Builder
{
    public class Chef
    {
        public void makePizza(IPizzaBuilder builder)
        {
            builder.MakeDough();
            builder.AddIngredients();
        }
    }
}