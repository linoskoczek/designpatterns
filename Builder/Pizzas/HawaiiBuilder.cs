using Builder.Doughs;
using Builder.Ingredients;

namespace Builder.Pizzas
{
    public class HawaiiBuilder : IPizzaBuilder
    {
        private readonly Pizza _pizza = new Pizza("Hawaii");
        
        public void MakeDough()
        {
            _pizza.Dough = new NewYork();
        }

        public void AddIngredients()
        {
            _pizza.AddIngredient(new Ham(1));
            _pizza.AddIngredient(new Pineapple(1));
            _pizza.AddIngredient(new Cheese(2));
        }

        public Pizza GetResult()
        {
            return _pizza;
        }
    }
}