namespace Builder.Pizzas
{
    public interface IPizzaBuilder
    {
        void MakeDough();
        void AddIngredients();
        Pizza GetResult();
    }
}