using Builder.Doughs;
using Builder.Ingredients;

namespace Builder.Pizzas
{
    public class CapriciosaBuilder : IPizzaBuilder
    {
        private readonly Pizza _pizza = new Pizza("Capriciosa");

        public void MakeDough()
        {
            _pizza.Dough = new Neapolitan();
        }

        public void AddIngredients()
        {
            _pizza.AddIngredient(new Ham(1));
            _pizza.AddIngredient(new Champignon(1));
            _pizza.AddIngredient(new Cheese(2));
        }

        public Pizza GetResult()
        {
            return _pizza;
        }
    }
}