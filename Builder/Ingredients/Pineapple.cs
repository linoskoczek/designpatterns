namespace Builder.Ingredients
{
    public class Pineapple : Ingredient
    {
        public Pineapple(ushort number) : base(number) {}

        protected override string GetName()
        {
            return "Pineapple";
        }
    }
}