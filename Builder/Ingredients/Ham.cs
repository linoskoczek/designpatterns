namespace Builder.Ingredients
{
    public class Ham : Ingredient
    {
        public Ham(ushort number) : base(number)
        {
        }

        protected override string GetName()
        {
            return "Ham";
        }
    }
}