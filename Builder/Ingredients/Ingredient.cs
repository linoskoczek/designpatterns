namespace Builder.Ingredients
{
    public abstract class Ingredient
    {
        private readonly ushort Number;
        
        public Ingredient(ushort number)
        {
            Number = number;
        }

        protected abstract string GetName();

        public string GetAll()
        {
            return Number + "x " + GetName();
        }
    }
}