namespace Builder.Ingredients
{
    public class Cheese : Ingredient
    {
        public Cheese(ushort number) : base(number)
        {
        }

        protected override string GetName()
        {
            return "Cheese";
        }
    }
}