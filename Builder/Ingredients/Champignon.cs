using Builder.Ingredients;

namespace Builder
{
    public class Champignon : Ingredient
    {
        public Champignon(ushort number) : base(number)
        {
        }

        protected override string GetName()
        {
            return "Champignon";
        }
    }
}