﻿using System;
using Builder.Pizzas;

namespace Builder
{
    class Program
    {
        private static readonly Chef Chef = new Chef();
        static void Main(string[] args)
        {
            Console.WriteLine("Mamma Mia, welcome to my Pizzeria!");
            
            HawaiiExample();
            CapriciosaExample();
        }

        private static void HawaiiExample()
        {
            Console.WriteLine("Ordering world's best type of pizza:");
            IPizzaBuilder hawaiiBuilder = new HawaiiBuilder();
            Chef.makePizza(hawaiiBuilder);
            Pizza hawaii = hawaiiBuilder.GetResult();

            Console.WriteLine(hawaii);
        }

        private static void CapriciosaExample()
        {
            Console.WriteLine("Ordering Capriciosa:");
            IPizzaBuilder capriciosaBuilder = new CapriciosaBuilder();
            Chef.makePizza(capriciosaBuilder);
            Pizza capriciosa = capriciosaBuilder.GetResult();

            Console.WriteLine(capriciosa);
        }
    }
}