namespace Builder.Doughs
{
    public interface IDough
    {
        string GetName();
    }
}