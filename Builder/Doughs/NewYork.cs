namespace Builder.Doughs
{
    public class NewYork : IDough
    {
        public string GetName()
        {
            return "New York style";
        }
    }
}