namespace Builder.Doughs
{
    public class Neapolitan : IDough
    {
        public string GetName()
        {
            return "Neapolitan style";
        }
    }
}