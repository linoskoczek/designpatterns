using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using Builder.Doughs;
using Builder.Ingredients;

namespace Builder
{
    public class Pizza
    {
        private readonly IList<Ingredient> _ingredients = new List<Ingredient>();
        internal IDough Dough;
        private readonly string _name;

        public Pizza(string name)
        {
            _name = name;
        }

        internal void AddIngredient(Ingredient ingredient)
        {
            _ingredients.Add(ingredient);
        }

        public override string ToString()
        {
            return _ingredients.Aggregate("[" + _name + "]\nDough: " + Dough.GetName() + "\nIngredients:\n", 
                (current, ingredient) => current + "- " + ingredient.GetAll() + "\n");
        }
    }
}